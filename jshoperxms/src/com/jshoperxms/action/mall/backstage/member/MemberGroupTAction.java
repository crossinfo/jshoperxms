package com.jshoperxms.action.mall.backstage.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.MemberGroupT;
import com.jshoperxms.service.MemberGroupTService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/member/membergroup")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class MemberGroupTAction extends BaseTAction {

	@Resource
	private MemberGroupTService memberGroupTService;

	/**
	 * 检查会员分组名称是否存在
	 * 
	 * @return
	 */
	public boolean checkMemberGroupT() {
		if (StringUtils.isBlank(this.getName())) {
			return false;
		}
		String name = StringUtils.trim(this.getName());
		Criterion criterion = Restrictions.eq("name", name);
		MemberGroupT mgt = this.memberGroupTService.findOneByCriteria(MemberGroupT.class, criterion);
		if (mgt != null) {
			return false;
		}
		return true;

	}

	/**
	 * 保存会员分组信息
	 * 
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveMemberGroupT() {
		if (checkMemberGroupT()) {
			MemberGroupT mgt = new MemberGroupT();
			mgt.setId(this.getSerial().Serialid(Serial.MEMBERGROUP));
			mgt.setName(this.getName());
			mgt.setAttrs(this.getArrts());
			mgt.setCreatorid(StringUtils.trim(BaseTools.getAdminCreateId()));
			mgt.setStatus(DataUsingState.USING.getState());
			mgt.setCreatetime(BaseTools.getSystemTime());
			mgt.setUpdatetime(BaseTools.getSystemTime());
			mgt.setVersiont(1);
			this.memberGroupTService.save(mgt);
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 查询所有会员分组
	 * 
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(name = "json", type = "json") })
	public String findAllMemberGroupT() {
		Criterion criterion = Restrictions.eq("status", DataUsingState.USING.getState());
		List<MemberGroupT> list = this.memberGroupTService.findByCriteria(MemberGroupT.class, criterion);
		if (!list.isEmpty()) {
			beanlists = new ArrayList<MemberGroupT>();
			beanlists = list;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 分页查询会员分组信息
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findMemberGroupTByPage() {
//		Map<String, Object> params = ActionContext.getContext().getParameters();
//		for (Map.Entry<String, Object> entry : params.entrySet()) {
//			String key = entry.getKey();
//			Object value = entry.getValue();
//			System.out.println("key:" + key + " value: " + Arrays.toString((String[]) value));
//		}
		// 检测是否需要搜索内容
		this.findDefaultAllMemberGroupT();
		return JSON;
	}

	public void processMemberGroupTList(List<MemberGroupT> list) {
		for (Iterator<MemberGroupT> it = list.iterator(); it.hasNext();) {
			MemberGroupT mgt = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", mgt.getId());
			cellMap.put("name", mgt.getName());
			cellMap.put("updatetime", BaseTools.formateDbDate(mgt.getUpdatetime()));
			cellMap.put("status", BaseEnums.DataUsingState.getName(mgt.getStatus()));
			cellMap.put("versiont", mgt.getVersiont());
			data.add(cellMap);
		}
	}

	public void findDefaultAllMemberGroupT() {
		int currentPage = 1;
		if (this.getStart() != 0) {
			currentPage = this.getStart() / this.getLength() == 1 ? 2 : this.getStart() / this.getLength() + 1;
		}
		int lineSize = this.getLength();
		Criterion criterion = Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered = recordsTotal = this.memberGroupTService.countfindAll(MemberGroupT.class);
		List<MemberGroupT> list = this.memberGroupTService.findByCriteriaByPage(MemberGroupT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processMemberGroupTList(list);
	}

	/**
	 * 根据ID查询会员分组信息
	 * 
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOneMemberGroupT() {
		if (StringUtils.isBlank(this.getMemberGroupId())) {
			return JSON;
		}
		MemberGroupT memberGroupT = this.memberGroupTService.findByPK(MemberGroupT.class, this.getMemberGroupId());
		if (memberGroupT != null) {
			bean = memberGroupT;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 修改会员分组信息
	 * 
	 * @return
	 */
	@Action(value = "update", results = { @Result(name = "json", type = "json") })
	public String updateMemberGroupT() {
		MemberGroupT memberGroupT = this.memberGroupTService.findByPK(MemberGroupT.class, this.getMemberGroupId());
		if ((memberGroupT != null && checkMemberGroupT()) || memberGroupT.getName().equals(this.getName())) {
			memberGroupT.setName(this.getName());
			memberGroupT.setAttrs(this.getArrts());
			memberGroupT.setStatus(this.getStatus());
			this.memberGroupTService.update(memberGroupT);
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 删除会员分组信息
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delMemberGroupT() {
		String ids[]=StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for(String s:ids){
			MemberGroupT memberGroupT = this.memberGroupTService.findByPK(MemberGroupT.class, s);
			if (memberGroupT != null) {
				memberGroupT.setStatus(BaseEnums.DataUsingState.DEL.getState());
				memberGroupT.setUpdatetime(BaseTools.getSystemTime());
				this.memberGroupTService.update(memberGroupT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}
	
	
	private String ids;
	private String memberGroupId;
	private MemberGroupT bean;
	private List<MemberGroupT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;
	private String name;
	private String status;
	private String arrts;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public MemberGroupT getBean() {
		return bean;
	}

	public void setBean(MemberGroupT bean) {
		this.bean = bean;
	}

	public List<MemberGroupT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<MemberGroupT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getArrts() {
		return arrts;
	}

	public void setArrts(String arrts) {
		this.arrts = arrts;
	}

	public String getMemberGroupId() {
		return memberGroupId;
	}

	public void setMemberGroupId(String memberGroupId) {
		this.memberGroupId = memberGroupId;
	}

}
