package com.jshoperxms.action.mall.frontstage.index;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.service.BrandTService;

@Namespace("/mall/brand")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class BrandTAction extends BaseTAction{
	@Resource
	private BrandTService brandTService;
	
	
}
