package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsSpecificationsProductRpT;
import com.jshoperxms.service.GoodsSpecificationsProductRpTService;

@Service("goodsSpecificationsProductRpTService")
@Scope("prototype")
public class GoodsSpecificationsProductRpTServiceImpl extends
		BaseTServiceImpl<GoodsSpecificationsProductRpT> implements
		GoodsSpecificationsProductRpTService {

}
