package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.MemberT;
import com.jshoperxms.service.MemberTService;

@Service("memberTService")
@Scope("prototype")
public class MemberTServiceImpl extends BaseTServiceImpl<MemberT> implements MemberTService{

}
