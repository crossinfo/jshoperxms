package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.PaymentM;
import com.jshoperxms.service.PaymentMService;



@Service("paymentMService")
@Scope("prototype")
public class PaymentMServiceImpl extends BaseTServiceImpl<PaymentM> implements PaymentMService {
	
}
