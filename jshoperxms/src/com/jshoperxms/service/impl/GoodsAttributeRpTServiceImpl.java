package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsAttributeRpT;
import com.jshoperxms.service.GoodsAttributeRpTService;

@Service("goodsAttributeRpTService")
@Scope("prototype")
public class GoodsAttributeRpTServiceImpl extends BaseTServiceImpl<GoodsAttributeRpT> implements GoodsAttributeRpTService {

}
