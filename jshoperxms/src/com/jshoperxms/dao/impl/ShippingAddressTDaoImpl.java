package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.ShippingAddressTDao;
import com.jshoperxms.entity.ShippingAddressT;


@Repository("shippingAddressTDao")
public class ShippingAddressTDaoImpl extends BaseTDaoImpl<ShippingAddressT> implements ShippingAddressTDao {


	private static final Logger log = LoggerFactory.getLogger(ShippingAddressTDaoImpl.class);


}