package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.PostsTDao;
import com.jshoperxms.entity.PostsT;

@Repository("postsTDao")
public class PostsTDaoImpl extends BaseTDaoImpl<PostsT> implements PostsTDao {

	private static final Logger log = LoggerFactory.getLogger(PostsTDaoImpl.class);

}
