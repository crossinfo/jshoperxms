var mModule=angular.module("goodsbrandtypeModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/goodsbrandtypement',{
		templateUrl:'../admin/app/goods/goodsbrandtypement.html',
		controller:'goodsbrandtype'
	});
});

mModule.controller('goodsbrandtype',['$scope','$http',function($scope,$http){
	$scope.title="商品品牌类型列表";
}]);

